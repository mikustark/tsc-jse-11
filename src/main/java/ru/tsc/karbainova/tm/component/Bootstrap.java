package ru.tsc.karbainova.tm.component;

import ru.tsc.karbainova.tm.api.controller.ICommandController;
import ru.tsc.karbainova.tm.api.controller.IProjectController;
import ru.tsc.karbainova.tm.api.controller.ITaskController;
import ru.tsc.karbainova.tm.api.repository.ICommandRepository;
import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.api.service.ICommandService;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.api.service.ITaskService;
import ru.tsc.karbainova.tm.controller.CommandController;
import ru.tsc.karbainova.tm.controller.ProjectController;
import ru.tsc.karbainova.tm.controller.TaskController;
import ru.tsc.karbainova.tm.repository.CommandRepository;
import ru.tsc.karbainova.tm.repository.ProjectRepository;
import ru.tsc.karbainova.tm.repository.TaskRepository;
import ru.tsc.karbainova.tm.service.CommandService;
import ru.tsc.karbainova.tm.service.ProjectService;
import ru.tsc.karbainova.tm.service.TaskService;

import java.util.Scanner;

import static ru.tsc.karbainova.tm.constant.ArgumentConst.*;
import static ru.tsc.karbainova.tm.constant.ArgumentConst.VERSION;
import static ru.tsc.karbainova.tm.constant.TerminalConst.*;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ITaskController taskController = new TaskController(taskService);

    public void start(String[] args) {
        commandController.displayWelcome();
        run(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String param) {
        switch (param) {
            case HELP:
                commandController.displayHelp();
                break;
            case ABOUT:
                commandController.displayAbout();
                break;
            case INFO:
                commandController.showInfo();
                break;
            case VERSION:
                commandController.displayVersion();
                break;
            case COMMANDS:
                commandController.showCommands();
                break;
            case ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.displayErrorCommand();
        }
    }

    public void parseCommand(final String param) {
        switch (param) {
            case CMD_HELP:
                commandController.displayHelp();
                break;
            case CMD_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_INFO:
                commandController.showInfo();
                break;
            case CMD_VERSION:
                commandController.displayVersion();
                break;
            case CMD_COMMANDS:
                commandController.showCommands();
                break;
            case CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case CMD_EXIT:
                commandController.exit();
                break;
            case CLEAR_PROJECT:
                projectController.clearProject();
                break;
            case CREATE_PROJECT:
                projectController.createProject();
                break;
            case LIST_PROJECT:
                projectController.showProject();
                break;
            case LIST_TASK:
                taskController.showTask();
                break;
            case CLEAR_TASK:
                taskController.clearTask();
                break;
            case CREATE_TASK:
                taskController.createTask();
                break;
            case UPDATE_BY_ID_PROJECT:
                projectController.updateById();
                break;
            case UPDATE_BY_INDEX_PROJECT:
                projectController.updateByIndex();
                break;
            case REMOVE_BY_ID_PROJECT:
                projectController.removeById();
                break;
            case REMOVE_BY_INDEX_PROJECT:
                projectController.removeByIndex();
                break;
            case REMOVE_BY_NAME_PROJECT:
                projectController.removeByName();
                break;
            case FIND_BY_ID_PROJECT:
                projectController.showById();
                break;
            case FIND_BY_INDEX_PROJECT:
                projectController.showByIndex();
                break;
            case FIND_BY_NAME_PROJECT:
                projectController.showByName();
                break;
            case UPDATE_BY_ID_TASK:
                taskController.updateById();
                break;
            case UPDATE_BY_INDEX_TASK:
                taskController.updateByIndex();
                break;
            case REMOVE_BY_ID_TASK:
                taskController.removeById();
                break;
            case REMOVE_BY_INDEX_TASK:
                taskController.removeByIndex();
                break;
            case REMOVE_BY_NAME_TASK:
                taskController.removeByName();
                break;
            case FIND_BY_ID_TASK:
                taskController.showById();
                break;
            case FIND_BY_INDEX_TASK:
                taskController.showByIndex();
                break;
            case FIND_BY_NAME_TASK:
                taskController.showByName();
                break;
            default:
                commandController.displayErrorArg();
        }
    }

    public void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        parseArg(param);
        System.exit(0);
    }
}
