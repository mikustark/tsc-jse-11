package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.model.Task;

import java.util.List;

public interface ITaskRepository {
    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findById(String id);

    Task findByIndex(int index);

    Task findByName(String name);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(int index);
}
