package ru.tsc.karbainova.tm.service;

import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(String name) {
        if (name == null || name.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
    }

    @Override
    public void create(String name, String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = projectRepository.findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = projectRepository.findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    @Override
    public Project findById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id);
    }

    @Override
    public Project findByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }
}
